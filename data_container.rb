require_relative 'user'

class DataContainer
	
	def initialize(data)
		@data= data
	end

	def storing
		user_name_column=0
		password_column=1
		permission_column=2

		$i=0
		$limit=@data.length

		@d1=Array.new
		@user_store=Array.new

		while $i<$limit do

			@d1[$i]=User.new(@data[$i][user_name_column],@data[$i][password_column],@data[$i][permission_column])
			@user_store<<@d1[$i].store_data

			$i+=1	
		end
	end

	def get_data
		return @user_store
	end
end
