require 'set'
require 'csv'

class Comparison

	def initialize(data,input)
		@data=data
		@input=input
	end

	def compare_data
		
		@username_column=0
		@password_column=1
		@permission_column=2

		
		for j in 0..(@input.length-1)
			username_compare(@input[j])
			puts "\n\n"
			
		end
	end

	def username_compare(input)
		 
		check=0
		
		begin
			input_data_row=input

			for i in 0..(@data.length-1)
				
				if input_data_row[@username_column]==(@data[i][@username_column])
					check=1		
					puts "User Name =>#{input_data_row[@username_column]}, Matched "		
					password_compare(input_data_row,i)
					break
				end
			end

			if check==0
				puts "User Name => #{input_data_row[@username_column]} doesn't exist in the Database"
			end

		rescue
			puts "Error in comparing User name"
			exit
		end
	end

	def password_compare(input,j)
		
		begin
			input_data_row=input
			input_password=input_data_row[@password_column]
			
			if input_password==@data[j][@password_column]
				puts "Password => Matched"
				permission_compare(input_data_row,j)
			else
				puts "Password => Doesn't Match"
			end
		rescue
			puts "Error in Password Comparison"
			exit
		end 		
	end

	def permission_compare(input,k)
		
		begin
			input_data_row=input
			input_permission=CSV.parse(input_data_row[@permission_column])

			data_permission=CSV.parse(@data[k][@permission_column])

			input_set=Set.new(input_permission)
			data_set=Set.new(data_permission)
			
			if input_set.subset?data_set
				puts "Permission => Matched"
				puts "User => #{input_data_row[@username_column]} Allowed"
			else
				puts "Permission => Doesn't Match"
			end
		rescue
			puts "Error in Permission Comparison"
		end
	end
end
			
