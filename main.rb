
require_relative 'input_reader'
require_relative 'data_generator'
require_relative 'data_container'
require_relative 'user.rb'
require_relative 'comparison'


data_file_name="data.csv"
data_read=InputReader.new(data_file_name)
data_read.check_file
data_extract_from_file=data_read.data_extract_file

data_extract=DataGenerate.new(data_extract_from_file)
data_extract.data_generate
extracted_user_data=data_extract.user_data

data_store=DataContainer.new(extracted_user_data)
data_store.storing
data=data_store.get_data


input_file_name="input.csv"
input_read=InputReader.new(input_file_name)
input_read.check_file
input_extract_from_file=input_read.data_extract_file

input_extract=DataGenerate.new(input_extract_from_file)
input_extract.data_generate
extracted_input_data=input_extract.user_data

input_store=DataContainer.new(extracted_input_data)
input_store.storing
input=input_store.get_data


compare=Comparison.new(data,input)
compare.compare_data
#puts extracted_input_data