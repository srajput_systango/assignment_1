class DataGenerate

	def initialize(data)
		@data=data
	end

	def data_generate
		@temporary_user_data=Array.new
		
		begin 
			$i=0
			$limit=@data.length

			@user_name_column=0
			@password_column=1
			@permission_column=2

			while $i<$limit do

				@temporary_user_data<<[@data[$i][@user_name_column],@data[$i][@password_column],@data[$i][@permission_column]]
				$i+=1
			end
		
		rescue
			puts "Error in Generating the Data"
			exit
		end

	end

	def user_data
		return @temporary_user_data
	end
end
