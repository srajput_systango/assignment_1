require 'csv'

class InputReader
	
	def initialize(file_name)
		@file_name=file_name
	end

	def check_file
		
		begin
			if File.exists?(@file_name) or File.zero?(@file_name)
				return true
			else
				raise 'Error with file, Program is Terminating'
				
			end
		rescue Exception => e
			puts e.message	
			puts e.backtrace.inspect
			exit
		end
	end

	def data_extract_file
		@data=CSV.read(@file_name,headers:true)
		return @data
	end
end